# Project Tasks Overview
    1. Terraform - Created EC2 instances
    2. Written plugin configuration for aws_ec2 user
    3. Adjusted TF config for public DNS assignment
    4. Adjusted Ansible playbook and ansible.cfg to use 
       dynamic hosts from AWS
    5. Added a 4th server and re-executed Ansible
       playbook
    6. Targeted only specific servers in your AWS
       region (filter + keyed_groups)
# Steps of the project
1. Create AWS EC2 instance with Terraform
```
   Create aws ec2 instance with Terraform
   $ terraform init
   $ terraform apply
```
2. Configure ansible inventory file to connect to AWS EC2 instance
```
Adjust Ansible Inventory File (hosts) to configure ec2 instance

Added [docker_server] to inventory file

Create playbooks--docker-deploy.yaml
    play 1: install python3 & docker & docker-compose
    play 2: start docker container
```
3. On ec2 server
    - install docker and docker-compose
    - copy docker-compose file to server
    - start docker container by running the docker-compose file

```
$ docker-compose -f docker-compose.yaml up
```






    


-----------------------------------------
### Dynamic Inventory
When we comstantly needs to spin up and shut down multiple hosts at the same time.

e.g. auto-scaling to accomodate for business demands

### Inventory Plugin vs Inventory Script
Plugins over scripts
- inventory plugins make use of Ansible features
- inventory plugins written in YAML
- inventory scripts written in Python

### To see list of available plugins

    $ ansible-doc -t inventory -l"

### use plugins to configure dynamic inventory
1. in ansible.cfg file, enable aws_ec2 plugins
2. write plugin configuration. we name it "inventory_aws_ec2.yaml"
(file name must end with "aws_ec2")

### To display or dump the configured inventory as Ansible sees it

    $ ansible-inventory -i inventory_aws_ec2.yaml --list
    $ ansible-inventory -i inventory_aws_ec2.yaml --graph

### Assign public DNS to ec2 instances
In main.tf, configure public DNS on vpc level (not instance level)
add "enable_dns_hostnames = true"

### Configure Ansible to use dynamic inventory
In playbook, we can put either "all" or "aws_ec2" for hosts.
In ansible.cfg file, configure credentials to aws

### Execute the playbook
When in ansible.cfg, inventory = hosts:
    $ ansible-playbook -i inventory_aws_ec2.yaml docker-deploy-new-user.yaml

When in ansible.cfg, inventory = inventory_aws_ec2.yaml
    $ ansible-playbook docker-deploy-new-user.yaml

### Target only specific servers
playbook A for dev-server
playbook B for test-server

in inventory_aws_ec2.yaml
add:

filters:
  tag:Name: test*

then, execute: 
    $ ansible-inventory -i inventory_aws_ec2.yaml --graph
    
---------------------------------------------------------------

